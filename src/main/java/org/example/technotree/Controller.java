package org.example.technotree;

import java.util.ArrayList;
import java.util.List;

import org.example.technotree.entity.Post;
import org.example.technotree.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.google.gson.Gson;

@RestController
@RequestMapping("/api/v1")
public class Controller {

	private static final String URL_2 = "https://jsonplaceholder.typicode.com/posts";

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	Gson gson;
	
	@GetMapping("/details/url/2")
	public ResponseEntity<Post[]> getFilteredDetails() {

		ResponseEntity<String> response = restTemplate.exchange(URL_2, HttpMethod.GET, HttpEntity.EMPTY, String.class);

		Post[] details = gson.fromJson(response.getBody(), Post[].class);

		/** Filter/update response, solution 1 (change response type to ResponseEntity<List<Post>) */
		List<Post> listPost = new ArrayList<>();
		for (Post dt : details) {
			if (dt.getId() == 1) {
				dt.setBody("Id ".concat(String.valueOf(dt.getId())));
				listPost.add(dt);
			}
		}

		return ResponseEntity.ok().body(details);
	}


}
