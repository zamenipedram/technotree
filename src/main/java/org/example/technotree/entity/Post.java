package org.example.technotree.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Table(name = "tbl_post")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Post {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "body")
    private String body;
    @Column(name = "userId")
    private int userId;

    public String toString(){
        return this.userId+" | "+this.id +" | "+this.title+" | "+this.body;
    }
}
